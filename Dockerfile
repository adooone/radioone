FROM ubuntu:latest

RUN apt-get update && \
    apt-get -y install sudo

RUN adduser --disabled-password --gecos '' admin
RUN adduser admin sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

WORKDIR /home/icecast
USER admin
# ************DO WHAT YOU WANT********************

RUN sudo apt-get -y install icecast2
RUN sudo mkdir logs
VOLUME ./logs
RUN sudo chown admin logs

# ************DO WHAT YOU WANT********************
COPY icecast.xml .

EXPOSE 6666

# docker run -p 6666:6666 --name radio --user admin -it --rm radio
# stat -c "%U %G" logs
CMD sudo icecast2 -c icecast.xml